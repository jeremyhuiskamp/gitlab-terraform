terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.6.0"
    }
  }

  backend "http" {}
}

variable "gitlab_api_token" {
  // set TF_VAR_gitlab_api_token or add a terraform.tfvars with:
  // gitlab_api_token = "blah"
  type        = string
  description = "Gitlab api key with api write access"
  sensitive   = true
}

provider "gitlab" {
  token = var.gitlab_api_token
}

resource "gitlab_project" "controlled_by_terraform" {
  name             = "controlled-by-terraform"
  description      = "demo of a project that is configured via terraform"
  default_branch   = "main"
  visibility_level = "public"

  merge_method = "ff"
}

resource "gitlab_pipeline_trigger" "example_pipeline_trigger" {
  project     = gitlab_project.controlled_by_terraform.id
  description = "use this to trigger builds"
}
